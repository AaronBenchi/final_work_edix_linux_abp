#!/bin/bash

#Depuramos los pasos de la ejecución, creamos un directorio Data, trabajamos en el directorio Data
set -x
mkdir data
cd data

#1_Collect_data: Descargamos de una API el CSV de población a nuestro host anfitrión
wget -c https://datahub.io/core/population/r/population.csv 

#2_Clean_data: Filtramos solo los datos de España, obtenemos la “nº población”y “años”, lo tabulamos, guardamos con “data_spain_population.tsv”, eliminamos el csv.
grep 'Spain' population.csv | cut -d ',' -f3,4 | sed 's/,/\t/g' > data_spain_population.tsv

rm -fr population.csv

#3_Visualize_data: Representación gráfica de la población Española desde 1961-2018.

cat 'data_spain_population.tsv' | gnuplot -p -e "set terminal dumb; set xlabel 'Time [years]' ;set xrange [1950:2025]; set yrange [25000000:50000000];  plot '<cat' using 1:2 with line linewidth 3 notitle" 
