#!/bin/bash

sudo apt-get install -yes emacs25-nox
sudo apt-get install git
sudo apt-get install wget
sudo apt-get install grep
sudo apt-get install -yes gnuplot
sudo apt-get install tree
git init
git config --global user.name "AaronBenchi"
git config --global user.email "aaronsuscripciones@gmail.com"
sudo timedatectl set-timezone Europe/Madrid
chmod 755 process.sh

