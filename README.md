# title:	"Pasos a seguir para ejecutar la maquina virtual Vagrant y reproducir tabajo final"
# author: 	"by [Aaron Benchiheub Pérez](https://www.linkedin.com/in/aaronbp/)"
# mail:    	"aaronbenchiheubperez@gmail.com"
# linkedin:	"aaronbp"
# twitter:  "@bp_aaron"
# gitlab:  	“AaronBenchi"
# date:     "23/06/20"


## Set up: Lo único que tiene que hacer es escribir en la terminal de tu ordenador la siguiente instrucción:

vagrant up

vagrant ssh 


## Execute data precess: una vez arrancada la maquina virtual ejecuta el comando siguiente:
/vagrant/process.sh